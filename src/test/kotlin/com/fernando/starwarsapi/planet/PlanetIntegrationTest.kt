package com.fernando.starwarsapi.planet

import com.fernando.starwarsapi.planetSearchApi.PlanetSearchApiService
import com.mongodb.BasicDBObject
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.`when`
import org.mockito.Mockito.eq
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.web.client.RestTemplate


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class PlanetIntegrationTest {

    @Autowired private lateinit var mvc: MockMvc
    @Autowired private lateinit var mongoTemplate: MongoTemplate
    @Autowired private lateinit var planetRepository: PlanetRepository
    @MockBean private lateinit var restTemplateMock: RestTemplate

    @After
    fun cleanUp() {
        mongoTemplate.collectionNames
                .filterNot { it.startsWith("system.") }
                .forEach { mongoTemplate.getCollection(it).deleteMany(BasicDBObject()) }
    }

    @Test
    fun creating_a_planet() {
        val json = "{ \"name\": \"My Planet\", \"climate\": \"cold\", \"terrain\": \"rocky\" }"
        val result = PlanetSearchApiService.Result("My Planet", listOf("Film 1", "Film 2"))
        val page = PlanetSearchApiService.Page(null, listOf(result))

        `when`(restTemplateMock.getForObject(eq("planets?search=My Planet"), any(Class::class.java)))
                .thenReturn(page)

        mvc.perform(post("/planets").contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andExpect(status().isOk)

        val savedPlanets: MutableIterable<Planet> = planetRepository.findAll()

        assertThat("Create only 1 planet", savedPlanets.count(), `is`(1))

        val savedPlanet = savedPlanets.first()

        assertThat("Name is correct", savedPlanet.name, `is`("My Planet"))
        assertThat("Climate is correct", savedPlanet.climate, `is`("cold"))
        assertThat("Terrain is correct", savedPlanet.terrain, `is`("rocky"))
        assertThat("Number of films is correct", savedPlanet.numFilms, `is`(2))
    }

    @Test
    fun get_a_planet_by_id() {

        val savedPlanet = planetRepository.save(Planet("My planet", "cold", "rocky", 3))

        mvc.perform(get("/planets/${savedPlanet.id}"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id", `is`(savedPlanet.id)))
                .andExpect(jsonPath("$.name", `is`(savedPlanet.name)))
                .andExpect(jsonPath("$.climate", `is`(savedPlanet.climate)))
                .andExpect(jsonPath("$.terrain", `is`(savedPlanet.terrain)))
                .andExpect(jsonPath("$.numFilms", `is`(savedPlanet.numFilms)))
    }

    @Test
    fun find_planets_by_name() {
        val planets = listOf(
                Planet("My planet", "cold", "rocky", 1),
                Planet("Other planet", "cold", "rocky", 2),
                Planet("My planet 2", "cold", "rocky", 3)
        )

        planetRepository.saveAll(planets)

        mvc.perform(get("/planets?name=My planet&page=0&size=5&sort=name,asc"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.numberOfElements", `is`(2)))
                .andExpect(jsonPath("$.size", `is`(5)))
                .andExpect(jsonPath("$.number", `is`(0)))
                .andExpect(jsonPath("$.content[0].name", `is`("My planet")))
                .andExpect(jsonPath("$.content[0].numFilms", `is`(1)))
                .andExpect(jsonPath("$.content[1].name", `is`("My planet 2")))
                .andExpect(jsonPath("$.content[1].numFilms", `is`(3)))
    }

    @Test
    fun deleting_a_planet() {
        val savedPlanet1 = planetRepository.save(Planet("My planet", "cold", "rocky", 2))
        val savedPlanet2 = planetRepository.save(Planet("My planet 2", "cold", "rocky", 2))

        mvc.perform(delete("/planets/${savedPlanet1.id}"))
                .andExpect(status().isNoContent)

        val planetsInDataBase = planetRepository.findAll()
        assertThat(planetsInDataBase.count(), `is`(1))
        assertThat(planetsInDataBase.first().id, `is`(savedPlanet2.id))
    }
}