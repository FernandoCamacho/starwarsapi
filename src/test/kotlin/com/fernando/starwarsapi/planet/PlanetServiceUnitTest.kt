package com.fernando.starwarsapi.planet

import com.fernando.starwarsapi.planetSearchApi.PlanetSearchApiService
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations.initMocks

class PlanetServiceUnitTest {

    @InjectMocks private lateinit var planetService: PlanetService
    @Mock private lateinit var planetRepositoryMock: PlanetRepository
    @Mock private lateinit var planetSearchApiServiceMock: PlanetSearchApiService

    @Before
    fun setup() = initMocks(this)

    @Test
    fun planet_service_should_can_films_api_to_get_total_films() {
        `when`(planetSearchApiServiceMock.findPlanetNumFilms("name")).thenReturn(2)
        doAnswer { it.arguments.first() }.`when`(planetRepositoryMock).save(any(Planet::class.java))

        val createdPlanet = planetService.createPlanet(PlanetInputDto("name", "climate", "terrain"))

        assertThat(createdPlanet.name, `is`("name"))
        assertThat(createdPlanet.climate, `is`("climate"))
        assertThat(createdPlanet.terrain, `is`("terrain"))
        assertThat(createdPlanet.numFilms, `is`(2))

        verify(planetSearchApiServiceMock, only()).findPlanetNumFilms("name")
        verify(planetRepositoryMock, only()).save(any(Planet::class.java))
    }
}