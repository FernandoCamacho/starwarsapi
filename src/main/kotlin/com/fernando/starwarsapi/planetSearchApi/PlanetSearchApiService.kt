package com.fernando.starwarsapi.planetSearchApi

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class PlanetSearchApiService(
        private val restTemplate: RestTemplate,
        @Value("\${star.wars.base.api}") private val baseUrl: String) {

    data class Page(val next: String?, val results: List<Result>)
    data class Result(val name: String, val films: List<String>)

    fun findPlanetNumFilms(planetName: String): Int? {
        val url = "${baseUrl}planets?search=$planetName"
        return getNumFilms(url, planetName)
    }

    private fun getNumFilms(url: String, planetName: String): Int? {
        val page = restTemplate.getForObject(url, Page::class.java)
        val numFilms = page?.results?.find { it.name.equals(planetName, ignoreCase = true)}?.films?.size

        return when {
            numFilms != null -> numFilms
            page?.next != null -> getNumFilms(page.next, planetName)
            else -> null
        }
    }
}