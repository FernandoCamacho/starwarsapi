package com.fernando.starwarsapi.planet

import com.fernando.starwarsapi.planetSearchApi.PlanetSearchApiService
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class PlanetService (
        private val planetSearchApiService: PlanetSearchApiService,
        private val planetRepository: PlanetRepository) {

    fun findAllPlanetsByName(name: String, pageParams: Pageable) = planetRepository.findAllByName(name, pageParams)

    fun findPlanetById(id: String) = planetRepository.findById(id)

    fun createPlanet(planetInputDto: PlanetInputDto): Planet {
        val numFilms = planetSearchApiService.findPlanetNumFilms(planetInputDto.name)
        val planet = Planet(planetInputDto.name, planetInputDto.climate, planetInputDto.terrain, numFilms)
        return planetRepository.save(planet)
    }

    fun deletePlanet(id: String) = planetRepository.deleteById(id)

    fun existsById(id: String) = planetRepository.existsById(id)
}