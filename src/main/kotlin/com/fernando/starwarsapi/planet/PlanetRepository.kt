package com.fernando.starwarsapi.planet

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository

interface PlanetRepository : PagingAndSortingRepository<Planet, String> {
    @Query(value = "{ 'name': {\$regex: ?0, \$options: 'i'} }")
    fun findAllByName(name: String, pageable: Pageable): Page<Planet>
}