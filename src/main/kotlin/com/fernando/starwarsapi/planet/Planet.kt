package com.fernando.starwarsapi.planet

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document(collection = "planets")
class Planet(val name: String, val climate: String, val terrain: String, val numFilms: Int?) {
    @Id
    val id: String? = null

    @CreatedDate
    val createdDate: Date = Date()

    @Version
    val version: Long = 0
}