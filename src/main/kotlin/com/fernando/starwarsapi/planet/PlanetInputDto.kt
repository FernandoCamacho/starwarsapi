package com.fernando.starwarsapi.planet

import javax.validation.constraints.NotBlank

class PlanetInputDto(
        @field:NotBlank
        val name: String,

        @field:NotBlank
        val climate: String,

        @field:NotBlank
        val terrain: String
)