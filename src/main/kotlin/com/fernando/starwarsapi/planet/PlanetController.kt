package com.fernando.starwarsapi.planet

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("planets")
class PlanetController(private val planetService: PlanetService) {

    class SearchParams(val name: String = "", page: Int, size: Int, sort: Sort?) {
        val pageParams = if (sort == null) PageRequest.of(page, size) else PageRequest.of(page, size, sort)
    }

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findAllPlanets(searchParams: SearchParams): Page<Planet> {
        return planetService.findAllPlanetsByName(searchParams.name, searchParams.pageParams)
    }

    @GetMapping("{id}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findPlanet(@PathVariable id: String): ResponseEntity<Planet> {
        val optionalPlanet = planetService.findPlanetById(id)

        return if (optionalPlanet.isPresent) ResponseEntity.ok(optionalPlanet.get())
        else ResponseEntity.notFound().build()
    }

    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createPlanet(@RequestBody @Valid planetInputDto: PlanetInputDto) = planetService.createPlanet(planetInputDto)

    @DeleteMapping("{id}")
    fun deletePlanet(@PathVariable id: String): ResponseEntity<Any> {
        if (!planetService.existsById(id))
            return ResponseEntity.notFound().build()

        planetService.deletePlanet(id)
        return ResponseEntity.noContent().build()
    }
}