package com.fernando.starwarsapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.config.EnableMongoAuditing

@SpringBootApplication
@EnableMongoAuditing
class StarWarsApiApplication

fun main(args: Array<String>) {
    runApplication<StarWarsApiApplication>(*args)
}
